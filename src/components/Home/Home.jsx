import React, {Component} from 'react';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            posts: []
        };
        fetch(process.env.REACT_APP_API_URL)
            .then(result => (result.json()))
            .then(posts => {
                this.setState({loading: false, posts});
            })
    }
    renderTable() {
        if (this.state.posts) {
            return this.state.posts.map(post => {
                return (
                    <tr key={post.id}>
                        <td>{post.title}</td>
                        <td>{post.body}</td>
                        <td>{post.id}</td>
                        <td>{post.userId}</td>
                    </tr>
                )
            })
        }
    }
    render() {
        return (
            <div className="ui container">
                <div className={this.state.loading ? "ui dimmer active" : ''}>
                    <div className="ui text loader">Loading</div>
                </div>
                <table className="ui table fixed striped t-head">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Body</th>
                        <th>#ID</th>
                        <th>#UserID</th>
                    </tr>
                    </thead>
                </table>
                <div className="scroll-table">
                    <table className="ui table fixed striped">
                        <tbody>
                        {this.renderTable()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Home;